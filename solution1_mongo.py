import logging as log
import random
import string
from datetime import datetime
from pprint import pprint
import timeit
from functools import partial
from pymongo import MongoClient, errors

from init_data import places_mongo_db as places

log.basicConfig(filename='solution1.log', level=log.DEBUG, format='%(asctime)s - %(name)s - %(threadName)s -  %(levelname)s - %(message)s')

class GeoMongo:


    _sys_db = ['admin', 'local', 'config']

    def __init__(self, host='localhost', ip=27017, db_name='', timeout_ms=1000, collection_name=''):
        """
        Check the DB connection
        :param host: 'localhost' by default
        :type host: string
        :param ip: '27017' by default
        :type ip: integer
        :param db_name: the name of database
        :type db_name: string
        :param timeout_ms: how long (in
            milliseconds) the driver will wait for a response
        :type timeout_ms: integer
        :param collection_name: MongoDB stores documents in collections.
        Collections are analogous to tables in relational databases.
        :type collection_name: string
        """
        self._client = MongoClient(host, ip, serverSelectionTimeoutMS=timeout_ms)
        self._db = self._client[db_name]
        self._col = collection_name
        log.info(f'__init__: param {self.__dict__}')

    def _collection(self, collection_name=""):
        """
        Select the proper collection forfurther actions
        :param collection_name: MongoDB stores documents in collections.
        Collections are analogous to tables in relational databases.
        :type collection_name: string
        """
        log.info(f'collection_param: {self._db[collection_name]}')
        if self._col:
            return self._db[self._col]
        else: 
            return self._db[collection_name]

    def _db_connection_test(self, show_all_db=False):
        """
        Check the DB connection and show all other databases (if selected)
        """
        try:
            self._client.server_info()
            log.info(f'{datetime} : connection established')
            print(f'_db__connection_test : connection established')
            if show_all_db:
                log.info(f'_db__connection_test : databases: {self._client.list_database_names()}')
                print(f'_db__connection_test : databases: {self._client.list_database_names()}')
            db = self._client.db_name
            connection_test = True
            log.info(f'_db__connection_test : db: {self._client.db_name}')
            return connection_test, db
        except errors.ServerSelectionTimeoutError as err:
            log.error(f'_db_connection_test : {err}')
            return False

    def find_circular_region(self, collection_name, landmark=None, radius=None, latitude=None,
                             longitude=None, by_kilometer=True):
        """
        To find all places (documents) located im given circular region. When some landmark will be given,
        function ignore latitude and longitude hence left landmark value to None, in case of searching by latitude and longitude.
        :param latitude: is a geographic coordinate that specifies the north–south position
         of a point on the Earth's surface
        :type latitude: integer
        :param longitude: is a geographic coordinate that specifies the east–west position
         of a point on the Earth's surface, or the surface of a celestial body
        :type longitude: integer
        :param radius: a line segment between any point of a circle or sphere and its center.
        :type radius: integer
        :param collection_name: the collection from which data need to be updated
        :type collection_name: string
        :param landmark:
        :type landmark: string
        :param by_kilometer: kilometer is the default unit (True) if You want switch to miles use False as a argument
        :type by_kilometer: True/False
        """
        col = self._collection(collection_name)
        radius_calc = radius/6378.1 if by_kilometer else radius/3963.2
        radius_pam = 'km' if by_kilometer else 'miles'
        if landmark:
            try:
                latitude, longitude = col.find_one({"name": landmark})['location']['coordinates']
            except:
                log.warning(f'find_circular_region: there is no {landmark} in db')
                return []
        query_set = [[record['_id'], record['location']['coordinates'],record['name']]
                     for record in col.find({'location': {'$geoWithin': {'$centerSphere':
                                                                             [[latitude, longitude], radius_calc]}}})]
        log.info(f'find_circular_region : all points in radius {radius:g} {radius_pam} {query_set[:2]}')

        # Simply for testing purpose, check the logfile.log file.
        print(f'Searching all paints in {radius} {radius_pam} from {landmark if landmark else [latitude, longitude]}')
        for en, el in enumerate(query_set):
            en +=1
            print(en, el[2])
        print()
        return query_set

    def find_records(self, collection_name, place_name=None, latitude=None, longitude=None):
        """
        To find places within a given names or latitudes/longitudes
        :param place_name: searched place
        :type place_name: string
        :param latitude: is a geographic coordinate that specifies the north–south
         position of a point on the Earth's surface
        :type latitude: integer
        :param longitude: is a geographic coordinate that specifies the east–west
         position of a point on the Earth's surface, or the surface of a celestial body
        :type longitude: integer
        :param collection_name: the collection from which data need to be updated
        :type collection_name: string
        """
        col = self._collection(collection_name)
        if place_name:
            query_set = col.find({'name': place_name})
        if latitude and longitude:
            query_set = col.find({'latitude': latitude, 'longitude': 'longitude'})
        else:
            query_set = []
        log.info(f'find_records : results: {query_set}.')
        return query_set

    def delete_single_db(self, db_to_drop):
        """
        Drop db
        :param db_to_drop
        :type db_to_drop: string
        """
        if db_to_drop not in self._sys_db:
            self.client.drop_database(db_to_drop)
            db_list = self.client.list_database_names()
            if db_to_drop not in db_list:
                log.info(f'{db_to_drop} has been deleted successfully.{self.client.list_database_names()}')
                print(f'{db_to_drop} has been deleted successfully.{self.client.list_database_names()}')
        else:
            print('You are not allowed to delete {} db'.format(db_to_drop))

    def delete_collection(self, collection_name):
        """
        Delete a single collection
        :param collection_name: the collection from which data need to be updated
        :type collection_name: string
        """
        if collection_name in self._db.list_collection_names():
            self._db.drop_collection(collection_name)
            log.info(f'delete_collection : {collection_name} has been deleted')

    def delete_one_record(self, place_name, collection_name):
        """
        Delete a single element(document) from db
        :param collection_name: MongoDB stores documents in collections.
         Collections are analogous to tables in relational databases.
        :type collection_name: integer
        :param place_name: place to delete
        :type place_name: string
        """
        col = self._collection(collection_name)
        col.delete_one({"name": place_name})
        log.info(f'delete_one_record: record {place_name} has been deleted')

    def insert_many(self, obj, collection_name):
        """
        Insert an iterable of documents.
        :param obj: A iterable of documents to insert.
        :type obj: dictionary
        :param collection_name: MongoDB stores documents in collections.
         Collections are analogous to tables in relational databases.
        :type collection_name: integer
        """
        try:
            col = self._collection(collection_name)
            col.insert_many(obj)
            log.info(f'insert_many')
        except Exception as err:
            log.error(f'insert_many : {err}')

    def insert_one(self, place_name, latitude, longitude, collection_name):
        """
        Insert a single record (document).
        :param place_name: name
        :type place_name: string
        :param latitude: is a geographic coordinate that specifies the north–south
         position of a point on the Earth's surface
        :type latitude: integer
        :param longitude: is a geographic coordinate that specifies the east–west
         position of a point on the Earth's surface, or the surface of a celestial body
        :type longitude: integer
        :param collection_name: MongoDB stores documents in collections.
         Collections are analogous to tables in relational databases.
        :type collection_name: integer
        """
        try:
            col = self._collection(collection_name)
            col.insert_one({'name': place_name, 'location': {'type': 'Point',
                                                             'coordinates': [latitude, longitude]}})
            log.info(f'{col.find_one({"name":place_name})}')
        except Exception as err:
            log.error(err)

    def update_one(self, collection_name, old_place_name=None, new_place_name=None, latitude=None, longitude=None):
        """
        Update a single record with given parameters
        :param collection_name: MongoDB stores documents in collections.
         Collections are analogous to tables in relational databases.
        :type collection_name: integer
        :param old_place_name: actual name
        :type old_place_name: string
        :param new_place_name: the collection from which data need to be updated
        :type new_place_name: string
        :param latitude: is a geographic coordinate that specifies
         the north–south position of a point on the Earth's surface
        :type latitude: integer
        :param longitude: is a geographic coordinate that specifies
         the east–west position of a point on the Earth's surface, or the surface of a celestial body
        :type longitude: integer
        """

        col = self._collection(collection_name)

        if not (latitude and longitude):
            col.update_one({"name": old_place_name}, {"$set": {
                                                            "name": new_place_name}})
            log.info(f'update_one {col.find_one({"name": new_place_name})} has been updated ')
        if latitude and longitude and old_place_name and new_place_name:
            col.update_one({"name": old_place_name}, {"$set": {
                                                            "name": new_place_name,
                                                            "latitude": latitude,
                                                            "longitude": longitude}})
            log.info(f'update_one {col.find_one({"name": new_place_name})} has been updated ')
        if not latitude and longitude:
            col.update_one({"name": old_place_name}, {"$set": {"longitude": longitude}})
            log.info(f'update_one {col.find_one({"longitude": longitude})} has been updated ')

        if latitude and not longitude:
            col.update_one({"name": old_place_name}, {"$set": {"latitude": latitude}})
            log.info(f'update_one {col.find_one({"latitude": latitude})} has been updated ')


    def show_db_records(self, collection_name, r_limit=100):
        """
        Query the database
        :param collection_name: MongoDB stores documents in collections.
         Collections are analogous to tables in relational databases.
        :type collection_name: integer
        :param r_limit: limiting results to a certain subset of fields you can cut
        down on network traffic and decoding time.
        :type r_limit: integer
        """
        col = self._collection(collection_name)
        log.info(f'show_db_records')
        for record in col.find({}).limit(r_limit):
            pprint(record)

# for testing purpose
def random_place_name(length):
    log.info(f'random_place_name')
    return ''.join(random.choice(string.ascii_letters) for _ in range(length))

# for testing purpose
def random_geometries():
    log.info(f'random_geometries')
    return random.uniform(-180, 180), random.uniform(-90, 90)

if __name__ == "__main__":

    """prepare testing environment"""
    obj1 = GeoMongo(db_name='wroclaw_db')
    # obj1.insert_many(places, 'worth_to_see')
    # obj1.show_db_records('worth_to_see', r_limit=10)

    """just for testing purpose"""
    # x, y = random_geometries()
    # obj1.insert_one(random_place_name(8), x, y, 'worth_to_see')

    """delete specified collection"""
    # obj1.delete_collection('worth_to_see')
    # obj1.delete_collection('worth_to_see2323') # no error

    """some simple example of updating/renaming a single document"""
    # obj1.update_one(collection_name='worth_to_see', old_place_name='vDIKinPp', new_place_name='test')

    """GLOWNA CZESC ZADANIA"""
    # q1 = obj1.find_circular_region(radius=1, landmark='Hala Targowa',
    #                                collection_name='worth_to_see', by_kilometer=True)
    # q2 = obj1.find_circular_region(radius=0.159, landmark='Clearcode',
    #                                collection_name='worth_to_see', by_kilometer=True)
    # q3 = obj1.find_circular_region(longitude=17.0300958, latitude=51.1100463, radius=0.250,
    #                                collection_name='worth_to_see', by_kilometer=True)

    """delete/drop some single document"""
    # q4 = obj1.delete_one_record("Hala Targowa",'worth_to_see')

    """ find place by name"""
    # obj1.show_db_records('worth_to_see')
    # q4 = obj1.find_records(place_name='Lemoon Group', collection_name='worth_to_see')

    """ meassure the execution time"""
    sphere = [17.0309769, 51.1100765]
    time_func = partial(obj1.find_circular_region,collection_name='worth_to_see',radius=0.5, landmark=sphere, by_kilometer=True)
    print(timeit.timeit(time_func,number=1000))