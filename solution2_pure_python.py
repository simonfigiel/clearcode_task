import logging as log
import math
from functools import partial
import timeit
# from pprint import pprint
# from collections import namedtuple
from init_data import data_set_pure_python as places

log.basicConfig(filename='solution2.log', level=log.DEBUG, format='%(asctime)s - %(name)s - %(threadName)s -  %(levelname)s - %(message)s')

def extract_data_by_coordinates(points_coordinates):
    return (place for place in places for cord in points_coordinates if place['coordinates'] == cord)

def calculate_distance_between_points(point, sphere_point_coordinates, radius, unit_km = True):
    """
     Calculates the distance between given point and sphere point.
     :param point: the point for which the distance will be calculated
     :type point: list with coordinates (for example [17.0300958, 51.1100463])
     :param radius: radius
     :type radius: string
     :param sphere_point_coordinates: coordinates of given sphere center
     :type sphere_point_coordinates: string
     :param unit_km: kilometers (by default) or miles
     :type unit_km: Bool
     :return: the distance between given points
    """
    _radius = 6371 if unit_km else 3963.2

    latitude1, longitude1 = point
    latitude2, longitude2 = sphere_point_coordinates

    dist_lat = math.radians(latitude2 - latitude1)
    dist_lon = math.radians(latitude2 - latitude1)
    a = (math.sin(dist_lat / 2) * math.sin(dist_lat / 2) +
         math.cos(math.radians(latitude1)) * math.cos(math.radians(latitude2)) *
         math.sin(dist_lon / 2) * math.sin(dist_lon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    dist_km = _radius * c

    if dist_km <= radius:
        log.info(f'Distance between point: {point} and the sphere point {sphere_point_coordinates} is equal to {round(dist_km, 3)} km')
        return point
    else:
        log.info(f"Selected point: {point} is out of range. Radius {radius} dist_km: {round(dist_km,2)}")

def distance_between_single_point(point_id=None, point_name=None, point_coordinates=None, radius=0, sphere_point_coordinates=None,unit_km=True):
    """
     Extracts proper coordintes for given point.
     :param point_id: the id of some point for which the distance will be calculated
     :type point_id: integer
     :param point_name: the name of some point for which the distance will be calculated
     :type point_name: string
     :param point_coordinates: the coordinates of some point for which the distance will be calculated
     :type point_coordinates: string
     :param radius: radius
     :type radius: string
     :param sphere_point_coordinates: coordinates of given sphere center
     :type sphere_point_coordinates: string
     :param unit_km: kilometers (by default) or miles
     :type unit_km: Bool
     :return: returns a single point if the distance between given point and sphere is not
     greater than given radius
    """
    try:
        if point_id:
            point = next(iter(item for item in places if item['id'] == point_id), None)['coordinates']
        elif point_name:
            point = next(iter(item for item in places if item['name'] == point_name), None)['coordinates']
        else:
            point = point_coordinates
    except:
        log.error(f"{point_id if point_id else point_name} doesn't exist")
        return None

    calculate_distance_between_points(point, sphere_point_coordinates, radius, unit_km)
    return point

def calculate_all_points(radius=0, sphere_point_coordinates=None, unit_km=True):
    """
    Return all points located in given sphere point area
    :param radius: radius
    :type radius: string
    :param sphere_point_coordinates: coordinates of given sphere center
    :type sphere_point_coordinates: string
    :param unit_km: kilometers (by default) or miles
    :type unit_km: Bool
    :return: list of coordinates
    """
    cord_list = (obj['coordinates'] for obj in places)
    func1 = partial(calculate_distance_between_points, radius=radius,
                    sphere_point_coordinates=sphere_point_coordinates, unit_km=unit_km)
    matched_coordinates = list(filter(lambda cord:bool(cord) != False,(func1(point=cord) for cord in cord_list)))

    return extract_data_by_coordinates(matched_coordinates)

if __name__ == '__main__':
    #radius = 0.5 means 500m
    sphere = [17.0309769, 51.1100765]

    # single point by point_coordinates
    p1 = distance_between_single_point(point_id=None, point_name=None, point_coordinates=[17.0300958, 51.1100463],
                                       radius=1, sphere_point_coordinates=sphere,unit_km=True)
    # # single point by point_coordinates
    p2 = distance_between_single_point(point_id=None, point_name=None, point_coordinates=[17.0300958, 51.1100463],
                                       radius=0.01, sphere_point_coordinates=sphere, unit_km=True)
    # # single point by point_id
    p3 = distance_between_single_point(point_id=10, point_name=None, point_coordinates=None, radius=1,
                                       sphere_point_coordinates=[17.0309769, 51.1100765],unit_km=True)
    # single point by point_name
    p4 = distance_between_single_point(point_id=None, point_name='Klub PRL', point_coordinates=None, radius=1,
                                       sphere_point_coordinates=sphere, unit_km=True)
    # multiple point with given radius (1km) and sphere_point_coordinates
    p5 = list(calculate_all_points(radius=1, sphere_point_coordinates=sphere,unit_km=True))
    # multiple point with given radius (0.3 km) and sphere_point_coordinates
    p6 = list(calculate_all_points(radius=0.2, sphere_point_coordinates=sphere, unit_km=True))

    # meassure the execution time
    time_func = partial(calculate_all_points,radius=0.5, sphere_point_coordinates=sphere, unit_km=True)
    print(timeit.timeit(time_func,number=1000))